import React, { Component } from "react";
import { StyleSheet, View, ScrollView } from "react-native";
import { removeItem, removeAllItem, addItem } from "../../actions/cart";
import { connect } from "react-redux";
import {
  Header,
  Left,
  Button,
  Body,
  Title,
  Text,
  List,
  ListItem,
  Thumbnail,
  Badge,
  Right,
  Container
} from "native-base";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";

class Basket extends Component {
  constructor(props) {
    super(props);
  }
  formatMoney(n, c, d, t) {
    var c = isNaN((c = Math.abs(c))) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt((n = Math.abs(Number(n) || 0).toFixed(c)))),
      j = (j = i.length) > 3 ? j % 3 : 0;

    return (
      s +
      (j ? i.substr(0, j) + t : "") +
      i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
      (c
        ? d +
          Math.abs(n - i)
            .toFixed(c)
            .slice(2)
        : "")
    );
  }
  handleRemoveItem = item => {
    this.props.remove(item);
  };

  handleRemoveAllItem = item => {
    this.props.removeAll(item);
  };

  handleAddItem = (item, feeShip) => {
    var food = { value: item, feeShip: feeShip };
    this.props.add(food);
  };

  handleCheckout = () => {
    if (this.props.cart.length > 0) {
      Actions.checkout();
    }
  };

  totalPrice() {
    var sum = 0;
    if (this.props.cart.length > 0) {
      sum = this.props.cart.reduce((a, b) => a + b.value.price * b.quantity, 0);
    }
    return sum;
  }
  render() {
    return (
      <Container>
        <ScrollView>
          {this.props.cart.length > 0 ? (
            <List
              dataArray={this.props.cart}
              renderRow={item => (
                <ListItem thumbnail>
                  <Left>
                    <Thumbnail source={{ uri: item.value.image }} />
                    <Badge info style={{ marginLeft: -10, marginBottom: 10 }}>
                      <Text>{item.quantity}</Text>
                    </Badge>
                  </Left>
                  <Body>
                    <Text>{item.value.name}</Text>
                    <Text note numberOfLines={1}>
                      {this.formatMoney(item.value.price, 0) + "VND"}
                    </Text>
                  </Body>
                  <Right style={{flexDirection: "row", alignItems: "center", justifyContent: "center"}}>
                    <Button
                      icon
                      transparent
                      onPress={() => this.handleRemoveItem(item.value.id)}
                      onLongPress={() =>
                        this.handleRemoveAllItem(item.value.id)
                      }
                      style={{marginLeft: 10, marginRight: 10}}
                    >
                      <Icon name="minus" size={20} />
                    </Button>
                    <Button
                      icon
                      transparent
                      onPress={() =>
                        this.handleAddItem(item.value, item.feeShip)
                      }
                      style={{marginLeft: 10, marginRight: 10}}
                    >
                      <Icon name="plus" size={20} color="#2196F3" />
                    </Button>
                  </Right>
                </ListItem>
              )}
            />
          ) : (
            <View
              style={{
                justifyContent: "center",
                flexDirection: "row",
                display: "flex"
              }}
            />
          )}
        </ScrollView>
        <View style={styles.totalPrice}>
          <Text style={{ fontWeight: "bold", color: "black", marginLeft: 10 }}>
            Total(not included ship fee)
          </Text>
          <Text style={{ color: "gray", fontWeight: "bold", marginRight: 10 }}>
            {this.formatMoney(this.totalPrice(), 0) + "VND"}
          </Text>
        </View>
        <Button
          iconLeft
          full
          info
          onPress={this.handleCheckout}
          disabled={this.props.cart.length > 0 ? false : true}
        >
          <Text>Create order</Text>
        </Button>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  totalPrice: {
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: "space-between"
  }
});

const mapStateToProps = state => {
  return {
    cart: state.items.items
  };
};

const mapDispatchToProps = dispatch => {
  return {
    add: item => {
      dispatch(addItem(item));
    },
    remove: item => {
      dispatch(removeItem(item));
    },
    removeAll: item => {
      dispatch(removeAllItem(item));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Basket);
