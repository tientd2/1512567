import React, { Component } from "react";
import { Image, ImageBackground } from "react-native";
import { Actions } from "react-native-router-flux";
import {
  Container,
  Content,
  List,
  ListItem,
  Text,
  Footer,
  FooterTab,
  Button,
  Icon
} from "native-base";
import { connect } from "react-redux";
import { logoutAccount } from "../../actions/account";

class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      phone: "",
      address: ""
    };
  }

  handleLogout = () => {
    var item = {};
    this.props.logout(item);
    Actions.login();
  };

  componentDidMount() {
    this.makeRemodeRequest();
  }

  makeRemodeRequest = () => {
    fetch("https://food-delivery-server.herokuapp.com/getinfo", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.props.token}`
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          email: responseJSON.email,
          phone: responseJSON.phone,
          address: responseJSON.address
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  render() {
    return (
      <Container>
        <Content>
          <ImageBackground
            source={require("../../assets/images/info.jpg")}
            style={{
              height: 120,
              alignSelf: "stretch",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image
              square
              style={{ height: 80, width: 70 }}
              source={require("../../assets/images/default-user.png")}
            />
            <Text style={{ color: "#fff" }}>{this.state.email}</Text>
          </ImageBackground>
          <List>
            <ListItem button onPress={() => Actions.drawerClose()}>
              <Text>Home</Text>
            </ListItem>
            <ListItem button onPress={() => Actions.basket()}>
              <Text>Basket</Text>
            </ListItem>
            <ListItem button onPress={() => Actions.userInfo()}>
              <Text>User Info</Text>
            </ListItem>
          </List>
        </Content>
        <Footer>
          <FooterTab>
            <Button
              full
              style={{ backgroundColor: "#2196F3" }}
              onPress={() => this.handleLogout()}
            >
              <Icon name="sign-out" type="FontAwesome" />
              <Text style={{ color: "#fff" }}>Log out</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    id: state.account.id,
    token: state.account.token
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: item => {
      dispatch(logoutAccount(item));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SideMenu);
