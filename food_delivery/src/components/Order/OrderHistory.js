import React, { Component } from "react";
import {
  Alert,
  ActivityIndicator,
  RefreshControl,
  ListView
} from "react-native";
import {
  Container,
  Content,
  List,
  ListItem,
  Body,
  Right,
  Text,
  Button,
  Icon,
  Left,
  Thumbnail
} from "native-base";

import moment from "moment";

import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";

class OrderHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
      isLoading: false,
      restaurant: [],
      refreshing: false,
      idx: 0
    };
    moment.updateLocale("en", {
      relativeTime: {
        future: "in %s",
        past: "%s ago",
        s: "seconds",
        ss: "%ss",
        m: "a minute",
        mm: "%dm",
        h: "an hour",
        hh: "%dh",
        d: "a day",
        dd: "%dd",
        M: "a month",
        MM: "%dM",
        y: "a year",
        yy: "%dY"
      }
    });
  }

  handleRefresh = () => {
    this.setState(
      {
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  makeRemoteRequest = () => {
    this.setState({ isLoading: true });
    fetch("https://food-delivery-server.herokuapp.com/order/getAll", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.props.token}`
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({ orders: responseJSON });
        if (this.state.refreshing) {
          this.setState({ restaurant: [], idx: 0 });
        }
        if (this.state.orders.length > 0) {
          for (var i = 0; i < this.state.orders.length; i++) {
            this.getRestaurantInfo(this.state.orders[i].idRestaurant);
          }
        } else {
          this.setState({ isLoading: false, refeshing: false });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  getRestaurantInfo(id) {
    fetch(`https://food-delivery-server.herokuapp.com/restaurant/getMenu/${id}`)
      .then(response => response.json())
      .then(responseJSON => {
        if (id != null) {
          var restaurantInfo = {
            id: id,
            value: responseJSON
          };
          this.state.restaurant.unshift(restaurantInfo);
        }
        this.state.idx++;
        if (this.state.idx == this.state.orders.length) {
          this.setState({ isLoading: false, refeshing: false });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  formatMoney(n, c, d, t) {
    var c = isNaN((c = Math.abs(c))) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt((n = Math.abs(Number(n) || 0).toFixed(c)))),
      j = (j = i.length) > 3 ? j % 3 : 0;

    return (
      s +
      (j ? i.substr(0, j) + t : "") +
      i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
      (c
        ? d +
          Math.abs(n - i)
            .toFixed(c)
            .slice(2)
        : "")
    );
  }

  getNameOfRestaurant(id) {
    for (var i = 0; i < this.state.restaurant.length; i++) {
      if (id === this.state.restaurant[i].id) {
        return this.state.restaurant[i].value.restaurant.name;
      }
    }
  }

  getThumbnailOfRestaurant(id) {
    for (var i = 0; i < this.state.restaurant.length; i++) {
      if (id === this.state.restaurant[i].id) {
        return this.state.restaurant[i].value.restaurant.image;
      }
    }
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  cancelOrder(id) {
    fetch(
      `https://food-delivery-server.herokuapp.com/order/cancelOrder/${id}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${this.props.token}`
        }
      }
    )
      .then(response => {
        if (!response.ok) {
          if (402 == response.status) {
            Alert.alert("Error", "Cannot cancel order.Please contact us");
          } else {
            Alert.alert("Error", "Something went wrong. Please try again");
          }
        }
      })
      .catch(error => {
        console.log(error);
      });
    this.handleRefresh();
  }

  render() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    return (
      <Container>
        <Content
          refreshControl={
            <RefreshControl
              refreshing={this.state.refeshing}
              onRefresh={this.handleRefresh}
            />
          }
        >
          {this.state.isLoading ? (
            <ActivityIndicator animating size="large" />
          ) : (
            <List
              rightOpenValue={-75}
              // dataArray={this.state.orders}
              dataSource={ds.cloneWithRows(this.state.orders)}
              renderRow={item => (
                <ListItem
                  onPress={() => Actions.orderDetail({ id: item.id })}
                  thumbnail
                >
                  <Left>
                    <Thumbnail
                      style={{ marginLeft: 10 }}
                      source={{
                        uri: this.getThumbnailOfRestaurant(item.idRestaurant)
                      }}
                    />
                  </Left>
                  <Body>
                    {item.idRestaurant == null ? (
                      <Text>Not available</Text>
                    ) : (
                      <Text>{this.getNameOfRestaurant(item.idRestaurant)}</Text>
                    )}
                    <Text note numberOfLines={1}>
                      {this.formatMoney(item.totalPrice, 0) + "VND"}
                    </Text>
                    {item.status < 0 ? (
                      <Text numberOfLines={1} style={{ color: "#f44336" }}>
                        Canceled
                      </Text>
                    ) : (
                      <Text numberOfLines={1} style={{ color: "#4CAF50" }}>
                        In processing
                      </Text>
                    )}
                  </Body>
                  <Right>
                    <Text note numberOfLines={1}>
                      {moment(item.date).fromNow()}
                    </Text>
                  </Right>
                </ListItem>
              )}
              renderRightHiddenRow={(item, secId, rowId, rowMap) => (
                <Button full danger onPress={() => this.cancelOrder(item.id)}>
                  <Icon active name="trash" />
                </Button>
              )}
            />
          )}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    id: state.account.id,
    token: state.account.token
  };
};

export default connect(mapStateToProps)(OrderHistory);
