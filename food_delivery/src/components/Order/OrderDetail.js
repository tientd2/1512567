import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Left,
  Button,
  Body,
  Title,
  Right,
  Text,
  List,
  ListItem,
  Thumbnail,
  Badge,
  View
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome5";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { ActivityIndicator, Alert } from "react-native";
import moment from "moment";

class OrderDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      order: {},
      restaurant: "",
      detail: [],
      food: [],
      idxFood: 0
    };
  }

  formatMoney(n, c, d, t) {
    var c = isNaN((c = Math.abs(c))) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt((n = Math.abs(Number(n) || 0).toFixed(c)))),
      j = (j = i.length) > 3 ? j % 3 : 0;

    return (
      s +
      (j ? i.substr(0, j) + t : "") +
      i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
      (c
        ? d +
          Math.abs(n - i)
            .toFixed(c)
            .slice(2)
        : "")
    );
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  getQuantity(idFood) {
    for (var i = 0; i < this.state.detail.length; i++) {
      if (this.state.detail[i].idFood == idFood) {
        return this.state.detail[i].quantity;
      }
    }
  }

  getNote(idFood) {
    for (var i = 0; i < this.state.detail.length; i++) {
      if (this.state.detail[i].idFood == idFood) {
        return this.state.detail[i].note;
      }
    }
  }

  makeRemoteRequest = () => {
    this.setState({ isLoading: true });
    const url = `https://food-delivery-server.herokuapp.com/order/getOrder/${
      this.props.id
    }`;
    fetch(url, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.props.token}`
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          order: responseJSON.order,
          detail: responseJSON.details
        });
        fetch(
          `https://food-delivery-server.herokuapp.com/restaurant/getMenu/${
            this.state.order.idRestaurant
          }`
        )
          .then(response => response.json())
          .then(responseJson => {
            this.setState({
              restaurant: responseJson.restaurant
            });
            for (var i = 0; i < this.state.detail.length; i++) {
              fetch(
                `https://food-delivery-server.herokuapp.com/food/${
                  this.state.detail[i].idFood
                }`
              )
                .then(response => response.json())
                .then(responseJSON => {
                  this.state.food.unshift(responseJSON.food);
                  this.state.idxFood++;
                  if (this.state.idxFood == this.state.detail.length) {
                    this.setState({ isLoading: false });
                  }
                });
            }
          });
      })
      .catch(error => console.log(error));
  };

  cancelOrder(id) {
    fetch(
      `https://food-delivery-server.herokuapp.com/order/cancelOrder/${id}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${this.props.token}`
        }
      }
    )
      .then(response => {
        if (response.ok) {
          Actions.pop(); 
        } else if (402 == response.status) {
          Alert.alert("Error", "Cannot cancel order.Please contact us");
        } else {
          Alert.alert("Error", "Something went wrong. Please try again");
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor="#2196F3"
          style={{ backgroundColor: "#2196F3" }}
        >
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon name="chevron-left" color="#fff" size={25} />
            </Button>
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={{ alignSelf: "center" }}>Detail</Title>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>
        {this.state.isLoading ? (
          <ActivityIndicator size="large" animating />
        ) : (
          <Content>
            <List>
              <ListItem>
                <Left>
                  <Text>Status Order</Text>
                </Left>
                {this.state.order.status < 0 ? (
                  <Text style={{ color: "#f44336" }}>Canceled</Text>
                ) : (
                  <Text style={{ color: "#4CAF50" }}>In processing</Text>
                )}
              </ListItem>
              <ListItem>
                <Left>
                  <Text>Restaurant</Text>
                </Left>
                <View style={{ flex: 1 }}>
                  <Text note numberOfLines={2}>
                    {this.state.restaurant.name}
                  </Text>
                </View>
              </ListItem>
              <ListItem>
                <Left>
                  <Text>Address</Text>
                </Left>
                <Text note numberOfLines={1}>
                  {this.state.order.address}
                </Text>
              </ListItem>
              <ListItem>
                <Left>
                  <Text>Phone Number</Text>
                </Left>
                <Text note numberOfLines={1}>
                  {this.state.order.phone}
                </Text>
              </ListItem>
              <ListItem>
                <Left>
                  <Text>Date Order</Text>
                </Left>
                <Text note numberOfLines={1}>
                  {moment(this.state.order.date).format("llll")}
                </Text>
              </ListItem>
              <ListItem>
                <Left>
                  <Text>Fee ship</Text>
                </Left>
                <Text note numberOfLines={1}>
                  {this.formatMoney(this.state.restaurant.feeShip, 0) + "VND"}
                </Text>
              </ListItem>
              <ListItem>
                <Left>
                  <Text>Total</Text>
                </Left>
                <Text numberOfLines={1}>
                  {this.formatMoney(this.state.order.totalPrice, 0) + "VND"}
                </Text>
              </ListItem>
            </List>
            <ListItem itemHeader>
              <Text>Ordered Items</Text>
            </ListItem>
            <List
              dataArray={this.state.food}
              renderRow={item => (
                <ListItem thumbnail>
                  <Left>
                    <Thumbnail square source={{ uri: item.image }} />
                  </Left>
                  <Body>
                    <Text>{item.name}</Text>
                    <Text note numberOfLines={1}>
                      {this.formatMoney(item.price, 0) + "VND"}
                    </Text>
                    <Text note numberOfLines={1}>{this.getNote(item.id)}</Text>
                  </Body>
                  <Right>
                    <Badge info>
                      <Text>{this.getQuantity(item.id)}</Text>
                    </Badge>
                  </Right>
                </ListItem>
              )}
            />
            {this.state.order.status >= 0 ? (
              <ListItem noBorder>
                <Body>
                  <Button danger block onPress={() => this.cancelOrder(this.state.order.id)}>
                    <Text>Cancel Order</Text>
                  </Button>
                </Body>
              </ListItem>
            ) : (
              <View />
            )}
          </Content>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.account.token
  };
};

export default connect(mapStateToProps)(OrderDetail);
