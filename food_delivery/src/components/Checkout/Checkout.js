import React, { Component } from "react";
import {
  Alert,
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Body,
  Title,
  Content,
  Left,
  Button,
  Right,
  Form,
  Item,
  Label,
  Input,
  Picker,
  Textarea,
  Text
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { clearAllItem } from "../../actions/cart";

class Checkout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalPrice: "",
      Street: "",
      idWard: "",
      idDistrict: "",
      phone: "",
      listDistrict: [],
      listWard: [],
      item: [],
      note: "",
      isLoading: false
    };
  }

  componentDidMount() {
    this.setState({ item: this.props.item, isLoading: true });
    fetch("https://food-delivery-server.herokuapp.com/district/getAll", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          listDistrict: responseJSON
        });
        fetch("https://food-delivery-server.herokuapp.com/getinfo", {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${this.props.token}`
          }
        })
          .then(response => response.json())
          .then(responseJSON => {
            this.setState({
              phone: responseJSON.phone == null ? "" : responseJSON.phone,
              idDistrict:
                responseJSON.address == null
                  ? 13096
                  : responseJSON.address.idDistrict,
              idWard:
                responseJSON.address == null ? 0 : responseJSON.address.idWard,
              Street:
                responseJSON.address == null ? "" : responseJSON.address.street,
            });
            fetch(
              `https://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=${
                this.state.idDistrict
              }`,
              {
                method: "GET",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json"
                }
              }
            )
              .then(response => response.json())
              .then(responseJSON => {
                this.setState({
                  listWard: responseJSON,
                  isLoading: false
                });
              })
              .catch(error => {
                console.log(error);
              });
          })
          .catch(error => {
            console.log(error);
          });
      })
      .catch(error => {
        console.log(error);
      });
  }

  formatMoney(n, c, d, t) {
    var c = isNaN((c = Math.abs(c))) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt((n = Math.abs(Number(n) || 0).toFixed(c)))),
      j = (j = i.length) > 3 ? j % 3 : 0;

    return (
      s +
      (j ? i.substr(0, j) + t : "") +
      i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
      (c
        ? d +
          Math.abs(n - i)
            .toFixed(c)
            .slice(2)
        : "")
    );
  }
  totalPrice() {
    var sum = 0;
    if (this.props.item.length > 0) {
      sum = this.props.item.reduce((a, b) => a + b.value.price * b.quantity, 0);
    }
    return sum;
  }

  onValueDistrictChange(value) {
    this.setState({
      idDistrict: value
    });
    fetch(
      `https://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=${value}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      }
    )
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          listWard: responseJSON
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onValueWardChange(value) {
    this.setState({
      idWard: value
    });
  }

  handleOrder = () => {
    var orderMap = new Map();
    for (var i = 0; i < this.state.item.length; i++) {
      var order = {
        idFood: this.state.item[i].value.id,
        quantity: this.state.item[i].quantity,
        price: this.state.item[i].value.price,
        feeShip: this.state.item[i].feeShip,
        note: this.state.note
      };
      if (orderMap.has(this.state.item[i].value.idRestaurant)) {
        var orderList = orderMap.get(this.state.item[i].value.idRestaurant);
        orderList.unshift(order);
      } else {
        var orderList = [];
        orderList.unshift(order);
        orderMap.set(this.state.item[i].value.idRestaurant, orderList);
      }
    }
    var isError = false;
    for (var [key, value] of orderMap) {
      var totalPriceOfOrder = 0;
      totalPriceOfOrder = value.reduce((a, b) => a + b.price * b.quantity, 0);
      if (value[0].feeShip !== null) {
        totalPriceOfOrder = totalPriceOfOrder + value[0].feeShip;
      }
      for (var i = 0; i < value.length; i++) {
        delete value[i].price;
        delete value[i].feeShip;
      }
      fetch("https://food-delivery-server.herokuapp.com/order/create", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${this.props.token}`
        },
        body: JSON.stringify({
          totalPrice: totalPriceOfOrder,
          Street: this.state.Street,
          idDistrict: this.state.idDistrict,
          idWard: this.state.idWard,
          phone: this.state.phone,
          item: value,
          idRestaurant: key
        })
      })
        .then(response => {
          if (!response.ok) {
            isError = true;
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
    if (!isError) {
      Alert.alert("Successful!", "Your order has been received");
      this.props.clearAll();
      Actions.pop();
    } else {
      Alert.alert("Oops!", "Something went wrong. Please try again");
    }
  };

  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor="#2196F3"
          style={{ backgroundColor: "#2196F3" }}
        >
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon name="chevron-left" color="#fff" size={25} />
            </Button>
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={{ alignSelf: "center" }}>Create Order</Title>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>
        {this.state.isLoading ? (
          <ActivityIndicator size="large" animating />
        ) : (
          <View style={{ flex: 1 }}>
            <ScrollView>
              <Form>
                <Item stackedLabel>
                  <Label>Phone Number</Label>
                  <Input
                    keyboardType="decimal-pad"
                    onChangeText={phone => this.setState({ phone: phone })}
                    value={this.state.phone}
                  />
                </Item>
                <Item stackedLabel>
                  <Label>Street</Label>
                  <Input
                    onChangeText={street => this.setState({ Street: street })}
                    autoCapitalize="words"
                    value={this.state.Street}
                  />
                </Item>
                <Item picker stackedLabel>
                  <Label>Ward</Label>
                  <Picker
                    mode="dropdown"
                    style={{ width: "100%" }}
                    selectedValue={this.state.idWard}
                    onValueChange={this.onValueWardChange.bind(this)}
                  >
                    {this.state.listWard.map(item => (
                      <Picker.Item
                        label={item.name}
                        value={item.id}
                        key={item.id}
                      />
                    ))}
                  </Picker>
                </Item>
                <Item picker stackedLabel>
                  <Label>District</Label>
                  <Picker
                    mode="dropdown"
                    style={{ width: "100%" }}
                    selectedValue={this.state.idDistrict}
                    onValueChange={this.onValueDistrictChange.bind(this)}
                  >
                    {this.state.listDistrict.map(item => (
                      <Picker.Item
                        label={item.name}
                        value={item.id}
                        key={item.id}
                      />
                    ))}
                  </Picker>
                </Item>

                <Item stackedLabel>
                  <Label>Note</Label>
                  <Textarea
                    rowSpan={5}
                    placeholder="Notice something..."
                    bordered
                    style={{ width: "100%" }}
                    onChangeText={note => this.setState({ note: note })}
                  />
                </Item>
              </Form>
            </ScrollView>
            <View style={styles.totalPrice}>
              <Text
                style={{ fontWeight: "bold", color: "black", marginLeft: 10 }}
              >
                Total (not included ship fee)
              </Text>
              <Text
                style={{ color: "gray", fontWeight: "bold", marginRight: 10 }}
              >
                {this.formatMoney(this.totalPrice(), 0) + "VND"}
              </Text>
            </View>
            <Button success block onPress={this.handleOrder}>
              <Text>Order</Text>
            </Button>
          </View>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  totalPrice: {
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: "space-between"
  }
});

const mapDispatchToProps = dispatch => {
  return {
    clearAll: () => {
      dispatch(clearAllItem());
    }
  };
};

const mapStateToProps = state => {
  return {
    id: state.account.id,
    token: state.account.token,
    item: state.items.items
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Checkout);
