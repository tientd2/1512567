import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar
} from 'react-native'
import LoginForm from "./LoginForm";

export default class Login extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor="#2196F3"
                />
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={require('../../assets/images/logo.png')}
                    />
                    <Text style={styles.title}>Food Delivery Now</Text>
                </View>
                <View style={styles.formContainer}>
                    <LoginForm/>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2196F3'
    },
    logoContainer: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    formContainer: {},
    logo: {
        width: 125,
        height: 100
    },
    title: {
        color: '#FFF',
        marginTop: 10,
        fontSize: 20,
        textAlign: 'center',
        opacity: 0.9
    }
});