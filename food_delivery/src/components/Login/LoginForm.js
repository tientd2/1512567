import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Text,
  ActivityIndicator,
  Alert
} from "react-native";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { loginAccount } from "../../actions/account";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      email: "",
      password: ""
    };
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator color="#ffffff" />
        </View>
      );
    }
    return (
      <View behavior="padding" style={styles.container}>
        <View style={styles.helperContainer}>
          <TouchableOpacity
            style={styles.helperItem}
            onPress={() => Actions.register()}
          >
            <Text style={styles.helperButton}>Sign up</Text>
          </TouchableOpacity>
          <Text style={styles.helperItem}> or </Text>
          <TouchableOpacity
            style={styles.helperItem}
            onPress={() => Actions.forgotPassword()}
          >
            <Text style={styles.helperButton}>Forgot password</Text>
          </TouchableOpacity>
        </View>
        <TextInput
          placeholder="Username"
          placeholderTextColor="rgba(255, 255, 255, 0.7)"
          style={styles.input}
          returnKeyType="next"
          onSubmitEditing={() => this.passwordInput.focus()}
          keyboardType="email-address"
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={email => this.setState({ email: email })}
        />
        <TextInput
          placeholder="Password"
          placeholderTextColor="rgba(255, 255, 255, 0.7)"
          style={styles.input}
          secureTextEntry
          returnKeyType="go"
          autoCapitalize="none"
          ref={input => (this.passwordInput = input)}
          onSubmitEditing={() => this.doLogin}
          onChangeText={password => this.setState({ password: password })}
        />
        <TouchableOpacity style={styles.buttonContainer} onPress={this.doLogin}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
      </View>
    );
  }

  handleVerifyAccount = () => {
    this.setState({
      isLoading: true
    });
    fetch("https://food-delivery-server.herokuapp.com/verify", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: this.state.email
      })
    }).then(response => {
      this.setState({ isLoading: false });
      if (response.ok) {
        Alert.alert("Message", "Email has been sent. Please check your inbox");
      } else if (400 == response.status) {
        Alert.alert("Error", "Email has been verified. Please try again");
      } else {
        Alert.alert("Error", "Something went wrong. Please try again");
      }
    });
  };

  doLogin = () => {
    this.setState({
      isLoading: true
    });
    fetch("https://food-delivery-server.herokuapp.com/login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password
      })
    })
      .then(response => {
        this.setState({ isLoading: false });
        if (response.ok) {
          var res = JSON.parse(response._bodyText);
          var item = { id: res.id, token: res.token };
          this.props.login(item);
          Actions.tabbar();
        } else if (401 == response.status) {
          Alert.alert(
            "Error",
            "It looks like you haven't verified your account. Do you want to send email again?",
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK", onPress: () => this.handleVerifyAccount() }
            ],
            { cancelable: false }
          );
        } else {
          response.json().then(responseJSON => {
            if (responseJSON != null) {
              Alert.alert("Error", responseJSON.msg);
              this.setState({ isLoading: false });
            }
          });
        }
      })

      .catch(error => {
        console.log(error);
      });
  };
}

const styles = StyleSheet.create({
  container: {
    padding: 20
  },
  input: {
    height: 45,
    backgroundColor: "#03A9F4",
    marginBottom: 10,
    color: "#FFF",
    paddingHorizontal: 10,
    borderRadius: 25
  },
  buttonContainer: {
    backgroundColor: "#3F51B5",
    paddingVertical: 15,
    marginBottom: 10,
    borderRadius: 25
  },
  helperContainer: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  helperItem: {
    padding: 5
  },
  helperButton: {
    color: "#FFFFFF",
    fontWeight: "500"
  },
  buttonText: {
    textAlign: "center",
    color: "#FFFFFF",
    fontWeight: "700"
  }
});

const mapDispatchToProps = dispatch => {
  return {
    login: item => {
      dispatch(loginAccount(item));
    }
  };
};

const mapStateToProps = state => {
  return {
    id: state.account.id,
    token: state.account.token,
    isLoggedIn: state.account.isLoggedIn
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);
