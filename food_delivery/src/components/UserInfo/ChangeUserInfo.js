import React, { Component } from "react";
import { Alert } from "react-native";
import {
  Header,
  Left,
  Button,
  Body,
  Title,
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Picker,
  Text
} from "native-base";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";

class ChangeUserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: 0,
      street: "",
      idDistrict: 0,
      idWard: 0,
      listDistrict: [],
      listWard: []
    };
  }

  componentDidMount() {
    fetch("https://food-delivery-server.herokuapp.com/district/getAll", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          listDistrict: responseJSON,
          idDistrict: responseJSON[0].id
        });
      })
      .catch(error => {
        console.log(error);
      });
    fetch(
      "https://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=13096",
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      }
    )
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          listWard: responseJSON,
          idWard: responseJSON[0].id
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onValueDistrictChange(value) {
    this.setState({
      idDistrict: value
    });
    fetch(
      `https://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=${value}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      }
    )
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          listWard: responseJSON,
          idWard: responseJSON[0].id
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onValueWardChange(value) {
    this.setState({
      idWard: value
    });
  }

  handleUpdateInfo = () => {
    if (
      this.state.phone === 0 ||
      this.state.idDistrict === 0 ||
      this.state.idWard === 0 ||
      this.state.street === ""
    ) {
      Alert.alert("Error", "Please update your info correctly");
      return;
    }

    fetch("https://food-delivery-server.herokuapp.com/updateInfo", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.props.token}`
      },
      body: JSON.stringify({
        phone: this.state.phone,
        idDistrict: this.state.idDistrict,
        idWard: this.state.idWard,
        street: this.state.street
      })
    })
      .then(response => response.json())
      .then(responseJSON => {
        Alert.alert("Message", responseJSON.msg);
        Actions.pop();
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor="#2196F3"
          style={{ backgroundColor: "#2196F3" }}
        >
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon name="chevron-left" color="#fff" size={25} />
            </Button>
          </Left>
          <Body>
            <Title>Change User Info</Title>
          </Body>
        </Header>
        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Phone Number</Label>
              <Input
                keyboardType="decimal-pad"
                onChangeText={phone => this.setState({ phone: phone })}
              />
            </Item>
            <Item stackedLabel>
              <Label>Street</Label>
              <Input
                onChangeText={street => this.setState({ street: street })}
                autoCapitalize="words"
              />
            </Item>
            <Item picker stackedLabel>
              <Label>District</Label>
              <Picker
                mode="dropdown"
                style={{ width: "90%" }}
                selectedValue={this.state.idDistrict}
                onValueChange={this.onValueDistrictChange.bind(this)}
              >
                {this.state.listDistrict.map(item => (
                  <Picker.Item
                    label={item.name}
                    value={item.id}
                    key={item.id}
                  />
                ))}
              </Picker>
            </Item>

            <Item picker stackedLabel>
              <Label>Ward</Label>
              <Picker
                mode="dropdown"
                style={{ width: "90%" }}
                selectedValue={this.state.idWard}
                onValueChange={this.onValueWardChange.bind(this)}
              >
                {this.state.listWard.map(item => (
                  <Picker.Item
                    label={item.name}
                    value={item.id}
                    key={item.id}
                  />
                ))}
              </Picker>
            </Item>
          </Form>
        </Content>
        <Button success block onPress={this.handleUpdateInfo}>
          <Text>Save</Text>
        </Button>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    id: state.account.id,
    token: state.account.token
  };
};

export default connect(mapStateToProps)(ChangeUserInfo);
