import React, { Component } from "react";
import { Alert } from "react-native";
import {
  Header,
  Left,
  Button,
  Body,
  Title,
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Text,
  ListItem
} from "native-base";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: ""
    };
  }

  handleChangePassword = () => {
    fetch("https://food-delivery-server.herokuapp.com/updatePassword", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.props.token}`
      },
      body: JSON.stringify({
        password: this.state.password
      })
    })
      .then(response => response.json())
      .then(responseJSON => {
        Alert.alert("Message", responseJSON.msg);
        Actions.pop();
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor="#2196F3"
          style={{ backgroundColor: "#2196F3" }}
        >
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon name="chevron-left" color="#fff" size={25} />
            </Button>
          </Left>
          <Body>
            <Title>Change Password</Title>
          </Body>
        </Header>
        <Content>
          <Form>
            <Item stackedLabel>
              <Label>New Password</Label>
              <Input
                onChangeText={password => this.setState({ password: password })}
                secureTextEntry
              />
            </Item>
          </Form>
          <ListItem noBorder>
            <Body>
              <Button success block onPress={this.handleChangePassword}>
                <Text>Change password</Text>
              </Button>
            </Body>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    id: state.account.id,
    token: state.account.token
  };
};

export default connect(mapStateToProps)(ChangePassword);
