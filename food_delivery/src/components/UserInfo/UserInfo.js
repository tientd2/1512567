import React, { Component } from "react";
import { Alert, PermissionsAndroid, ActivityIndicator } from "react-native";
import {
  Header,
  Button,
  Body,
  Title,
  Container,
  Content,
  Text,
  ListItem,
  Icon,
  Form,
  Item,
  Picker,
  Label,
  Input,
  Thumbnail
} from "native-base";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { logoutAccount } from "../../actions/account";
import { clearAllItem } from "../../actions/cart";
import { Avatar } from "react-native-elements";

class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: 0,
      street: "",
      idDistrict: 0,
      idWard: 0,
      listDistrict: [],
      listWard: [],
      email: "",
      password: "",
      avatar: "https://food-delivery-server.herokuapp.com/default-user.jpg",
      userName: "",
      isLoading: false
    };
  }

  handleLogout = () => {
    var item = {};
    this.props.logout(item);
    this.props.clearAll();
    Actions.login();
  };

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    this.setState({ isLoading: true });
    fetch("https://food-delivery-server.herokuapp.com/getinfo", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.props.token}`
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          email: responseJSON.email,
          phone: responseJSON.phone == null ? 0: responseJSON.phone,
          idDistrict: responseJSON.address == null ? 13096: responseJSON.address.idDistrict,
          idWard: responseJSON.address == null ? 0: responseJSON.address.idWard,
          street: responseJSON.address == null ? "": responseJSON.address.street,
          avatar: "https://" + responseJSON.avatarUrl,
          userName: responseJSON.userName == null ? "": responseJSON.userName,
        });
        fetch(
          `https://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=${
            this.state.idDistrict
          }`,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            }
          }
        )
          .then(response => response.json())
          .then(responseJSON => {
            this.setState({
              listWard: responseJSON
            });
            fetch(
              "https://food-delivery-server.herokuapp.com/district/getAll",
              {
                method: "GET",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json"
                }
              }
            )
              .then(response => response.json())
              .then(responseJSON => {
                this.setState({
                  listDistrict: responseJSON,
                  isLoading: false
                });
              })
              .catch(error => {
                console.log(error);
              });
          })
          .catch(error => {
            console.log(error);
          });
      })
      .catch(error => {
        console.log(error);
      });
  };

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Food Delivery Camera Permission",
          message:
            "Food Delivery needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera");
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  onValueDistrictChange(value) {
    this.setState({
      idDistrict: value
    });
    fetch(
      `https://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=${value}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      }
    )
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          listWard: responseJSON,
          idWard: responseJSON[0].id
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onValueWardChange(value) {
    this.setState({
      idWard: value
    });
  }

  handleUpdateInfo = () => {
    if (
      this.state.phone === 0 ||
      this.state.idDistrict === 0 ||
      this.state.idWard === 0 ||
      this.state.street === ""
    ) {
      Alert.alert("Error", "Please update your info correctly");
      return;
    }
    this.setState({ isLoading: true });
    fetch("https://food-delivery-server.herokuapp.com/updateInfo", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.props.token}`
      },
      body: JSON.stringify({
        phone: this.state.phone,
        idDistrict: this.state.idDistrict,
        idWard: this.state.idWard,
        street: this.state.street,
        userName: this.state.userName
      })
    })
      .then(response => {
        if (response.ok) {
          if (this.state.password !== "") {
            fetch("https://food-delivery-server.herokuapp.com/updatePassword", {
              method: "POST",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: `Bearer ${this.props.token}`
              },
              body: JSON.stringify({
                password: this.state.password
              })
            })
              .then(response => {
                this.setState({ isLoading: false });
                if (response.ok) {
                  response.json().then(responseJSON => {
                    Alert.alert("Message", responseJSON.msg);
                  });
                } else {
                  Alert.alert(
                    "Error",
                    "Something went wrong. Please try again"
                  );
                }
              })
              .catch(error => {
                console.log(error);
              });
          } else {
            this.setState({ isLoading: false });
            response.json().then(responseJSON => {
              Alert.alert("Message", responseJSON.msg);
            });
          }
        } else {
          Alert.alert("Error", "Something went wrong. Please try again");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor="#2196F3"
          style={{ backgroundColor: "#2196F3" }}
        >
          <Body style={{ flex: 1 }}>
            <Title style={{ alignSelf: "center" }}>User Info</Title>
          </Body>
        </Header>
        {this.state.isLoading ? (
          <ActivityIndicator size="large" animating />
        ) : (
          <Content>
            <Form>
              <Item stackedLabel>
                <Avatar
                  xlarge
                  rounded
                  source={{ uri: this.state.avatar }}
                  onPress={() => this.requestCameraPermission()}
                  activeOpacity={0.7}
                  containerStyle={{ marginTop: 10 }}
                />
                <Label>Email</Label>
                <Input
                  keyboardType="email-address"
                  onChangeText={email => this.setState({ email: email })}
                  value={this.state.email}
                  disabled
                />
              </Item>
              <Item stackedLabel>
                <Label>User Name</Label>
                <Input
                  onChangeText={userName => this.setState({ userName: userName })}
                  value={this.state.userName}
                />
              </Item>
              <Item stackedLabel>
                <Label>Phone Number</Label>
                <Input
                  keyboardType="decimal-pad"
                  onChangeText={phone => this.setState({ phone: phone })}
                  value={this.state.phone.toString()}
                />
              </Item>
              <Item stackedLabel>
                <Label>New Password (Optional)</Label>
                <Input
                  autoCapitalize="none"
                  secureTextEntry
                  onChangeText={password =>
                    this.setState({ password: password })
                  }
                />
              </Item>
              <Item stackedLabel>
                <Label>Street</Label>
                <Input
                  onChangeText={street => this.setState({ street: street })}
                  autoCapitalize="words"
                  value={this.state.street}
                />
              </Item>
              <Item picker stackedLabel>
                <Label>Ward</Label>
                <Picker
                  mode="dropdown"
                  style={{ width: "100%" }}
                  selectedValue={this.state.idWard}
                  onValueChange={this.onValueWardChange.bind(this)}
                >
                  {this.state.listWard.map(item => (
                    <Picker.Item
                      label={item.name}
                      value={item.id}
                      key={item.id}
                    />
                  ))}
                </Picker>
              </Item>
              <Item picker stackedLabel>
                <Label>District</Label>
                <Picker
                  mode="dropdown"
                  style={{ width: "100%" }}
                  selectedValue={this.state.idDistrict}
                  onValueChange={this.onValueDistrictChange.bind(this)}
                >
                  {this.state.listDistrict.map(item => (
                    <Picker.Item
                      label={item.name}
                      value={item.id}
                      key={item.id}
                    />
                  ))}
                </Picker>
              </Item>
            </Form>
            <ListItem noBorder>
              <Body>
                <Button info block onPress={this.handleUpdateInfo}>
                  <Text>Save Change</Text>
                </Button>
              </Body>
            </ListItem>
            <ListItem noBorder>
              <Body>
                <Button danger block onPress={this.handleLogout} iconLeft>
                  <Icon name="sign-out" type="FontAwesome" />
                  <Text>Logout</Text>
                </Button>
              </Body>
            </ListItem>
          </Content>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    id: state.account.id,
    token: state.account.token
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: item => {
      dispatch(logoutAccount(item));
    },
    clearAll: () => {
      dispatch(clearAllItem());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserInfo);
