import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Text,
  ActivityIndicator,
  Alert
} from "react-native";

export default class ForgotPasswordForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      email: ""
    };
  }
  render() {
    return (
      <View behavior="padding" style={styles.container}>
        {this.state.isLoading ? (
          <ActivityIndicator animating color="#fff" size="large" />
        ) : null}
        <Text style={styles.header}>Forgot password ?</Text>
        <TextInput
          placeholder="Email address"
          placeholderTextColor="rgba(255, 255, 255, 0.7)"
          style={styles.input}
          returnKeyType="go"
          keyboardType="email-address"
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={email => this.setState({ email: email })}
        />
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={this.doForgotPassword}
        >
          <Text style={styles.buttonText}>Reset password</Text>
        </TouchableOpacity>
      </View>
    );
  }

  doForgotPassword = () => {
    this.setState({ isLoading: true });
    fetch("https://food-delivery-server.herokuapp.com/forgetPassword", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: this.state.email
      })
    })
      .then(response => response.json())
      .then(responseJSON => {
        Alert.alert("Message", responseJSON.msg);
        this.setState({ isLoading: false });
      })
      .catch(error => {
        console.log(error);
      });
  };
}

const styles = StyleSheet.create({
  container: {
    padding: 20
  },
  header: {
    fontSize: 20,
    fontWeight: "500",
    padding: 10,
    color: "#FFFFFF"
  },
  input: {
    height: 40,
    backgroundColor: "#03A9F4",
    marginBottom: 10,
    color: "#FFF",
    paddingHorizontal: 10,
    borderRadius: 25
  },
  buttonContainer: {
    backgroundColor: "#3F51B5",
    paddingVertical: 15,
    marginBottom: 10,
    borderRadius: 25
  },
  buttonText: {
    textAlign: "center",
    color: "#FFFFFF",
    fontWeight: "700"
  }
});
