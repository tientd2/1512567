import React, {Component} from 'react'
import {StyleSheet, View, StatusBar, Image, TouchableOpacity} from 'react-native'
import Icon from "react-native-vector-icons/FontAwesome";
import {Actions} from "react-native-router-flux";
import ForgotPasswordForm from "./ForgotPasswordForm";

export default class ForgotPassword extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor="#2196F3"
                />
                <TouchableOpacity style={styles.backIcon} onPress={() => Actions.pop()}>
                    <Icon name="chevron-left" size={20} color="#FFF"/>
                </TouchableOpacity>
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={require('../../assets/images/forgot-password.png')}
                    />
                </View>
                <View style={styles.formContainer}>
                    <ForgotPasswordForm/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2196F3',
    },
    formContainer: {
        flexGrow: 1,
    },
    backIcon: {
        marginTop: 10,
        marginLeft: 20
    },
    logoContainer: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    logo: {
        width: 125,
        height: 125
    }
});