import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  TouchableOpacity
} from "react-native";
import RegisterForm from "./RegisterForm";
import Icon from "react-native-vector-icons/FontAwesome";
import { Actions } from "react-native-router-flux";

export default class Register extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor="#2196F3" />
        <TouchableOpacity style={styles.backIcon} onPress={() => Actions.pop()}>
          <Icon name="chevron-left" size={20} color="#FFF" />
        </TouchableOpacity>
        <View style={styles.formContainer}>
          <RegisterForm />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#2196F3"
  },
  formContainer: {
    paddingRight: 60,
    paddingLeft: 60,
    // justifyContent: "center",
    // alignContent: "center"
  },
  backIcon: {
    marginTop: 20,
    marginLeft: 20
  },
});
