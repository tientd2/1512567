import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  Alert
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

export default class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      email: "",
      password: "",
      passwordConfirm: ""
    };
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.isLoading ? (
          <ActivityIndicator
            animating
            color="#fff"
            size="large"
          />
        ) : null}
        <View style={styles.textInputContainer}>
          <Icon style={styles.icon} name="user" size={20} color="#FFF" />
          <TextInput
            style={styles.textInput}
            placeholder="Email"
            placeholderTextColor="rgba(255, 255, 255, 0.7)"
            autoCorrect={false}
            underlineColorAndroid={"transparent"}
            autoCapitalize={"none"}
            returnKeyType="next"
            blurOnSubmit={false}
            onSubmitEditing={() => this.passwordInput.focus()}
            onChangeText={email => this.setState({ email: email })}
          />
        </View>
        {/* <View style={styles.textInputContainer}>
                    <Icon style={styles.icon} name="address-card" size={20} color="#FFF"/>
                    <TextInput
                        style={styles.textInput}
                        placeholder="Full name"
                        placeholderTextColor="rgba(255, 255, 255, 0.7)"
                        autoCorrect={false}
                        underlineColorAndroid={'transparent'}
                        autoCapitalize={'words'}
                        returnKeyType="next"
                        blurOnSubmit={false}
                        ref={(input) => { this.fullnameInput = input; }}
                        onSubmitEditing={() => this.passwordInput.focus()}
                    />
                </View> */}
        <View style={styles.textInputContainer}>
          <Icon style={styles.icon} name="lock" size={20} color="#FFF" />
          <TextInput
            style={styles.textInput}
            placeholder="Password"
            placeholderTextColor="rgba(255, 255, 255, 0.7)"
            autoCorrect={false}
            underlineColorAndroid={"transparent"}
            secureTextEntry
            autoCapitalize={"none"}
            returnKeyType="next"
            blurOnSubmit={false}
            ref={input => {
              this.passwordInput = input;
            }}
            onSubmitEditing={() => this.confirmPasswordInput.focus()}
            onChangeText={password => this.setState({ password: password })}
          />
        </View>
        <View style={styles.textInputContainer}>
          <Icon style={styles.icon} name="lock" size={20} color="#FFF" />
          <TextInput
            style={styles.textInput}
            placeholder="Confirm Password"
            placeholderTextColor="rgba(255, 255, 255, 0.7)"
            autoCorrect={false}
            underlineColorAndroid={"transparent"}
            secureTextEntry
            autoCapitalize={"none"}
            returnKeyType="go"
            blurOnSubmit={false}
            ref={input => {
              this.confirmPasswordInput = input;
            }}
            onChangeText={passwordConfirm =>
              this.setState({ passwordConfirm: passwordConfirm })
            }
          />
        </View>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={this.doRegister}
        >
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    );
  }
  doRegister = () => {
    if (this.state.password === this.state.passwordConfirm) {
      this.setState({ isLoading: true });
      fetch("https://food-delivery-server.herokuapp.com/register", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: this.state.email,
          password: this.state.password
        })
      })
        .then(response => response.json())
        .then(responseJSON => {
          this.setState({ isLoading: false });
          if (responseJSON.hasOwnProperty('id')) {
            Alert.alert('Successful', "An email has been sent to your email address. Please check your inbox")
          }
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      Alert.alert("Error","Password does not matched");
    }
  };
}

const styles = StyleSheet.create({
  container: {
    alignSelf: "stretch"
  },
  textInputContainer: {
    flexDirection: "row"
  },
  icon: {
    padding: 10
  },
  textInput: {
    flex: 1,
    alignSelf: "stretch",
    height: 40,
    marginBottom: 20,
    color: "#FFF",
    borderBottomColor: "#F8F8F8",
    borderBottomWidth: 1
  },
  buttonText: {
    textAlign: "center",
    color: "#FFFFFF",
    fontWeight: "700"
  },
  buttonContainer: {
    backgroundColor: "#3F51B5",
    paddingVertical: 15,
    marginBottom: 10,
    borderRadius: 25
  }
});
