import React, { Component } from "react";
import Merchant from "./Merchant";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import {
  Container,
} from "native-base";

export default class Home extends Component {
  render() {
    return (
      <Container>
        <Merchant />
      </Container>
    );
  }
}
