import React, { Component } from "react";
import {
  View,
  FlatList,
  ImageBackground,
  Text,
  StyleSheet,
  ActivityIndicator,
  TouchableHighlight,
  Alert
} from "react-native";
import { Badge } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import { Item, Icon, Input, Button, Header } from "native-base";

export default class Merchant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
      page: 1,
      seed: 5,
      error: null,
      refreshing: false,
      isEnd: false,
      searchText: "",
      isSearching: false
    };
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed,
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };
  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  renderFooter = () => {
    if (!this.state.isLoading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    const url = `https://food-delivery-server.herokuapp.com/restaurant/getAll/${seed}&${page}`;
    this.setState({ isLoading: true });
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: page === 1 ? res.data : [...this.state.data, ...res.data],
          error: res.error || null,
          isLoading: false,
          refreshing: false,
          isEnd: res.next_page === -1 ? true : false
        });
      })
      .catch(error => {
        this.setState({ error, isLoading: false });
      });
  };
  renderItem({ item }) {
    if (item.hasOwnProperty("info")) {
      item = item.info;
    }
    return (
      <TouchableHighlight onPress={() => this.handleClickItem(item.id)}>
        <View style={styles.container}>
          <ImageBackground style={styles.image} defaultSource={require('../../assets/images/default-image.png')} source={{uri: item.image}}>
            <View style={styles.imageOverlay} />
          </ImageBackground>

          <View style={styles.textContainer}>
            <Badge containerStyle={styles.badge}>
              <Text style={styles.badgeText}>{item.rating}★</Text>
            </Badge>
            <Text style={styles.title}>{item.name}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
  handleSearch = () => {
    const url = `https://food-delivery-server.herokuapp.com/restaurant/search?name=${
      this.state.searchText
    }`;
    this.setState({ isLoading: true });
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.refs["searchInput"]._root.clear();
        if (res.length) {
          this.setState({
            isSearching: true,
            data: res,
            isLoading: false,
            refreshing: false,
            isEnd: true
          });
        } else {
          Alert.alert("Error", `${this.state.searchText} not found`);
        }
      })
      .catch(error => {
        this.setState({ error, isLoading: false });
      });
  };
  handleClickItem(id) {
    Actions.detail({id: id})
  }
  handleCancelSearch = () => {
    this.setState({ isSearching: false, data: [] }, () => this.handleRefresh());
  };
  render() {
    return (
      <View style={{ borderTopWidth: 0, borderBottomWidth: 0, flex: 1 }}>
        <Header
          androidStatusBarColor="#2196F3"
          style={{ backgroundColor: "#2196F3" }}
          searchBar
          rounded
        >
          <Item>
            {this.state.isSearching ? <Icon name="ios-arrow-back" onPress={() => this.handleCancelSearch()}/> : <Icon name="ios-search"/>}
            <Input
              placeholder="Search"
              onChangeText={searchText =>
                this.setState({ searchText: searchText })
              }
              onSubmitEditing={this.handleSearch}
              returnKeyType="search"
              clearButtonMode="unless-editing"
              clearTextOnFocus
              ref="searchInput"
            />
          </Item>
        </Header>
        <FlatList
          data={this.state.data}
          renderItem={this.renderItem.bind(this)}
          keyExtractor={item =>
            item.hasOwnProperty("info")
              ? item.info.id.toString()
              : item.id.toString()
          }
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={50}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "relative",
    backgroundColor: "rgba(0,0,0,0)",
    paddingTop: 2,
    paddingHorizontal: 2
  },
  textContainer: {
    position: "absolute",
    top: 10,
    bottom: 30,
    right: 10,
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "flex-end"
  },
  title: {
    color: "white",
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "right",
    lineHeight: 20,
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10
  },
  dateCreated: {
    color: "white",
    fontSize: 14,
    textAlign: "right"
  },
  badge: {
    backgroundColor: "rgba(52, 52, 52, 0.8)",
    borderRadius: 4,
    height: 30,
    padding: 6
  },
  badgeText: {
    color: "white",
    fontSize: 16,
    fontWeight: "bold"
  },
  image: {
    height: 250
  },
  imageOverlay: {
    backgroundColor: "rgba(0,0,0,0.3)",
    height: 250
  },
  bottomTextGroup: {
    top: 10
  }
});
