import React, { Component } from "react";
import {
  ScrollView,
  StyleSheet,
  View,
  ImageBackground,
  ActivityIndicator,
  Alert
} from "react-native";
import { ListItem } from "react-native-elements";
import { addItem, removeItem } from "../../actions/cart";
import { connect } from "react-redux";
import {
  Header,
  Left,
  Body,
  Button,
  Title,
  Text,
  Right,
  ListItem as ItemList,
  List,
  Thumbnail,
  Badge,
  Container,
  Icon as IconInput,
  Tab,
  Tabs,
  Item,
  Input,
  Form,
  Label
} from "native-base";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome5";
import Swiper from "react-native-swiper";
import Video from "react-native-video";
import moment from "moment";

var i = 0;
class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      restaurant: {},
      menu: [],
      resource: [],
      address: "",
      isLoading: false,
      isLoadingComment: false,
      comments: [],
      comment: "",
      name: ""
    };
    moment.updateLocale("en", {
      relativeTime: {
        future: "in %s",
        past: "%s ago",
        s: "seconds",
        ss: "%ss",
        m: "a minute",
        mm: "%dm",
        h: "an hour",
        hh: "%dh",
        d: "a day",
        dd: "%dd",
        M: "a month",
        MM: "%dM",
        y: "a year",
        yy: "%dY"
      }
    });
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  generateKey() {
    return i++;
  }

  makeRemoteRequest = () => {
    this.setState({ isLoading: true });
    const url = `https://food-delivery-server.herokuapp.com/restaurant/getMenu/${
      this.props.id
    }`;
    fetch(url)
      .then(res => res.json())
      .then(res => {
        var item = { url: res.restaurant.image, type: "image" };
        res.resource.unshift(item);
        this.setState({
          restaurant: res.restaurant,
          menu: res.menu,
          resource: res.resource,
          address: res.address,
          isLoading: false,
          isLoadingComment: true
        });
        fetch(
          `https://food-delivery-server.herokuapp.com/restaurant/getAllCommentById/${
            this.props.id
          }`
        ).then(response => {
          if (response.ok) {
            response.json().then(responseJSON => {
              this.setState({
                comments: responseJSON.result,
                isLoadingComment: false
              });
            });
          } else {
            Alert.alert("Error", "Cannot get comment of this restaurant");
          }
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  formatMoney(n, c, d, t) {
    var c = isNaN((c = Math.abs(c))) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt((n = Math.abs(Number(n) || 0).toFixed(c)))),
      j = (j = i.length) > 3 ? j % 3 : 0;

    return (
      s +
      (j ? i.substr(0, j) + t : "") +
      i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
      (c
        ? d +
          Math.abs(n - i)
            .toFixed(c)
            .slice(2)
        : "")
    );
  }

  handleAddItem = (item, feeShip) => {
    var food = { value: item, feeShip: feeShip };
    this.props.add(food);
  };

  handleRemoveItem = item => {
    this.props.remove(item);
  };

  handleComment = () => {
    if (this.state.comment == "") {
      Alert.alert("Error", "Please type your comment before send.");
      return;
    }
    fetch("https://food-delivery-server.herokuapp.com/restaurant/addComment", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.props.token}`
      },
      body: JSON.stringify({
        name: this.state.name == "" ? "Anonymous User" : this.state.name,
        idRestaurant: this.props.id,
        content: this.state.comment
      })
    })
      .then(response => {
        if (response.ok) {
          this.refs["commentInput"]._root.clear();
          this.setState({ isLoadingComment: true });
          fetch(
            `https://food-delivery-server.herokuapp.com/restaurant/getAllCommentById/${
              this.props.id
            }`
          )
            .then(response => {
              if (response.ok) {
                response.json().then(responseJSON => {
                  this.setState({
                    comments: responseJSON.result,
                    isLoadingComment: false
                  });
                });
              } else {
                Alert.alert("Error", "Cannot get comment of this restaurant");
              }
            })
            .catch(error => {
              console.log(error);
            });
        } else if (401 == response.status) {
          Alert.alert("Error", "Restaurant does not exist");
        } else {
          Alert.alert("Error", "Something went wrong. Please try again");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  totalPrice() {
    var sum = 0;
    if (this.props.cart.length > 0) {
      sum = this.props.cart.reduce((a, b) => a + b.value.price * b.quantity, 0);
    }
    return sum;
  }

  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor="#2196F3"
          style={{ backgroundColor: "#2196F3" }}
          hasTabs
        >
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon name="chevron-left" color="#fff" size={25} />
            </Button>
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={{ alignSelf: "center" }}>Detail</Title>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>
        {this.state.isLoading ? (
          <ActivityIndicator size="large" animating />
        ) : (
          <Tabs locked>
            <Tab
              heading="Info"
              tabStyle={{ backgroundColor: "#2196F3" }}
              activeTabStyle={{ backgroundColor: "#2196F3" }}
            >
              <View style={{ flex: 1 }}>
                <ScrollView>
                  <Swiper
                    height={240}
                    key={this.state.resource.length}
                    dot={
                      <View
                        style={{
                          backgroundColor: "rgba(0,0,0,.2)",
                          width: 5,
                          height: 5,
                          borderRadius: 4,
                          marginLeft: 3,
                          marginRight: 3,
                          marginTop: 3,
                          marginBottom: 3
                        }}
                      />
                    }
                    activeDot={
                      <View
                        style={{
                          backgroundColor: "#000",
                          width: 8,
                          height: 8,
                          borderRadius: 4,
                          marginLeft: 3,
                          marginRight: 3,
                          marginTop: 3,
                          marginBottom: 3
                        }}
                      />
                    }
                    paginationStyle={{
                      bottom: -20
                    }}
                  >
                    {this.state.resource.map(item => {
                      if (item.type === "image") {
                        return (
                          <ImageBackground
                            key={this.generateKey()}
                            source={{ uri: item.url }}
                            defaultSource={require("../../assets/images/default-image.png")}
                            style={{ left: 0, right: 0, height: 240 }}
                          />
                        );
                      } else {
                        return (
                          <Video
                            source={{
                              uri:
                                "http://techslides.com/demos/sample-videos/small.mp4"
                            }}
                            style={{ height: 240, left: 0, right: 0 }}
                            rate={1.0}
                            resizeMode={"cover"}
                            ignoreSilentSwitch={"obey"}
                            key={this.generateKey()}
                            muted={true}
                            repeat={true}
                          />
                        );
                      }
                    })}
                  </Swiper>
                  <View
                    style={{
                      marginTop: 20,
                      justifyContent: "flex-end",
                      alignItems: "flex-start"
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Badge
                        success
                        style={{ marginLeft: 10, marginRight: 10 }}
                      >
                        <Text style={{ fontWeight: "bold" }}>
                          {this.state.restaurant.rating}★
                        </Text>
                      </Badge>
                      <View style={{ flex: 1 }}>
                        <Text
                          style={{
                            fontWeight: "bold",
                            color: "#4CAF50",
                            fontSize: 18
                          }}
                          numberOfLines={2}
                        >
                          {this.state.restaurant.name}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: 5 }}>
                      <Icon
                        name="location-arrow"
                        size={20}
                        style={{ marginLeft: 20, marginRight: 20 }}
                        color="#2196F3"
                      />
                      <View style={{ flex: 1, paddingLeft: 5 }}>
                        <Text note style={{ color: "#2196F3" }}>
                          {this.state.address.address}
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <Icon
                      name="shipping-fast"
                      size={20}
                      color="#f44336"
                      style={{ marginLeft: 20, marginRight: 20 }}
                    />
                    <Text note>
                      {"Feeship: " +
                        this.formatMoney(this.state.restaurant.feeShip, 0) +
                        "VND"}
                    </Text>
                  </View>
                  <List
                    dataArray={this.state.menu}
                    renderRow={item => (
                      <ItemList thumbnail>
                        <Left>
                          <Thumbnail
                            source={{ uri: item.image }}
                            resizeMode="cover"
                            square
                          />
                        </Left>
                        <Body>
                          <Text>{item.name}</Text>
                          <Text note numberOfLines={1}>
                            {this.formatMoney(item.price, 0) + "VND"}
                          </Text>
                        </Body>
                        <Right>
                          <Button
                            icon
                            transparent
                            onPress={() =>
                              this.handleAddItem(
                                item,
                                this.state.restaurant.feeShip
                              )
                            }
                          >
                            <Icon name="plus" color="#2196F3" size={25} />
                          </Button>
                        </Right>
                      </ItemList>
                    )}
                  />
                </ScrollView>
                <View style={styles.cartContainer}>
                  <View>
                    {this.props.cart.length > 0 ? (
                      <View style={{ borderWidth: 1, backgroundColor: "#fff" }}>
                        <Text
                          style={{
                            fontWeight: "bold",
                            color: "black",
                            paddingLeft: 10
                          }}
                        >
                          Recent Ordered
                        </Text>
                        <ListItem
                          key={this.props.cart[this.props.recent].value.id}
                          title={this.props.cart[this.props.recent].value.name}
                          avatar={{
                            uri: this.props.cart[this.props.recent].value.image
                          }}
                          subtitle={
                            this.formatMoney(
                              this.props.cart[this.props.recent].value.price,
                              0
                            ) + "VND"
                          }
                          roundAvatar
                          rightIcon={{ name: "minus", type: "font-awesome" }}
                          onPressRightIcon={() =>
                            this.handleRemoveItem(
                              this.props.cart[this.props.recent].value.id
                            )
                          }
                          badge={{
                            value: this.props.cart[this.props.recent].quantity,
                            wrapperStyle: { marginRight: 10 }
                          }}
                        />
                      </View>
                    ) : (
                      <View />
                    )}
                    <View style={styles.totalPrice}>
                      <Text
                        style={{
                          fontWeight: "bold",
                          color: "black",
                          marginLeft: 10
                        }}
                      >
                        Total
                      </Text>
                      <Text
                        style={{
                          color: "gray",
                          fontWeight: "bold",
                          marginRight: 10
                        }}
                      >
                        {this.formatMoney(this.totalPrice(), 0) + "VND"}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </Tab>
            <Tab
              heading="Comment"
              tabStyle={{ backgroundColor: "#2196F3" }}
              activeTabStyle={{ backgroundColor: "#2196F3" }}
            >
              <View style={{ flex: 1 }}>
                <ScrollView>
                  {this.state.isLoadingComment ? (
                    <ActivityIndicator size="large" animating />
                  ) : (
                    <List
                      dataArray={this.state.comments}
                      renderRow={item => (
                        <ItemList>
                          <Body>
                            <Text note>{item.name}</Text>
                            <Text>{item.content}</Text>
                          </Body>
                          <Right>
                            <Text note numberOfLines={1}>
                              {moment(item.createAt).fromNow()}
                            </Text>
                          </Right>
                        </ItemList>
                      )}
                    />
                  )}
                </ScrollView>
                <Form>
                  <Item
                    regular
                    style={{
                      borderTopLeftRadius: 25,
                      borderTopRightRadius: 25
                    }}
                  >
                    <IconInput type="FontAwesome" name="user-o" />
                    <Input
                      placeholder="Your name"
                      onChangeText={name => this.setState({ name: name })}
                      onSubmitEditing={() =>
                        this.refs["commentInput"]._root.focus()
                      }
                      returnKeyType="next"
                      autoCapitalize="words"
                    />
                  </Item>
                  <Item regular>
                    <IconInput type="FontAwesome" name="comment-o" />
                    <Input
                      placeholder="Comment something..."
                      onChangeText={comment =>
                        this.setState({ comment: comment })
                      }
                      onSubmitEditing={this.handleComment}
                      returnKeyType="send"
                      clearTextOnFocus
                      ref="commentInput"
                    />
                  </Item>
                </Form>
              </View>
            </Tab>
          </Tabs>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  cartContainer: {
    justifyContent: "flex-start"
  },
  buttonContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#2196F3",
    paddingVertical: 15
  },
  totalPrice: {
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  buttonText: {
    textAlign: "center",
    color: "#FFFFFF",
    fontWeight: "700"
  },
  slide: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "transparent"
  }
});

const mapStateToProps = state => {
  return {
    cart: state.items.items,
    recent: state.items.recent
  };
};

const mapDispatchToProps = dispatch => {
  return {
    add: item => {
      dispatch(addItem(item));
    },
    remove: item => {
      dispatch(removeItem(item));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Detail);
