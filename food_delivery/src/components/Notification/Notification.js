import React, { Component } from "react";
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Text,
  Right,
  List,
  ListItem,
  Title
} from "native-base";

import { ActivityIndicator, RefreshControl } from "react-native";

import { connect } from "react-redux";

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: [],
      isLoading: false,
      refeshing: false
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }
  handleRefresh = () => {
    this.setState(
      {
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  makeRemoteRequest = () => {
    this.setState({ isLoading: true });
    fetch("https://food-delivery-server.herokuapp.com/getNoti", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.props.token}`
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          notifications: responseJSON,
          isLoading: false,
          refeshing: false
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor="#2196F3"
          style={{ backgroundColor: "#2196F3" }}
        >
          <Body style={{ flex: 1 }}>
            <Title style={{ alignSelf: "center" }}>Notification</Title>
          </Body>
        </Header>
        <Content
          refreshControl={
            <RefreshControl
              refreshing={this.state.refeshing}
              onRefresh={this.handleRefresh}
            />
          }
        >
          {this.state.isLoading ? (
            <ActivityIndicator size="large" animating />
          ) : (
            <List
              dataArray={this.state.notifications}
              renderRow={item => (
                <ListItem>
                  <Body>
                    <Text>{item.title}</Text>
                    <Text note>{item.content}</Text>
                  </Body>
                </ListItem>
              )}
            />
          )}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    id: state.account.id,
    token: state.account.token
  };
};

export default connect(mapStateToProps)(Notification);
