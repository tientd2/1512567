import { LOGIN, LOGOUT } from './types'

export const loginAccount = item => {
    return {
        type: LOGIN,
        id: item.id,
        token: item.token,
    }
}

export const logoutAccount = item => {
    return {
        type: LOGOUT,
        payload: item,
    }
}