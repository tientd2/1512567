export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const REMOVE_ITEM_ALL = 'REMOVE_ITEM_ALL';
export const CLEAR_ALL_ITEM = 'CLEAR_ALL_ITEM';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';