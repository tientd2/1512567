import { ADD_ITEM, REMOVE_ITEM, REMOVE_ITEM_ALL, CLEAR_ALL_ITEM } from './types'

export const addItem = item => {
    return {
        type: ADD_ITEM,
        payload: item
    }
}

export const removeItem = item => {
    return {
        type: REMOVE_ITEM,
        payload: item
    }
}

export const removeAllItem = item => {
    return {
        type: REMOVE_ITEM_ALL,
        payload: item
    }
}

export const clearAllItem = () => {
    return {
        type: CLEAR_ALL_ITEM
    }
}