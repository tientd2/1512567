import { LOGIN, LOGOUT } from "../actions/types";

const initialState = {
  id: '',
  token: '',
  isLoggedIn: false,
};

const accountReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN: {
      state = Object.assign({}, state, { id: action.id, token: action.token, isLoggedIn: true });
      return state;
    }
    case LOGOUT: {
      state = Object.assign({}, state, { id: '', token: '', isLoggedIn: false });
      return state;
    }
    default:
      return state;
  }
};

export default accountReducer;
