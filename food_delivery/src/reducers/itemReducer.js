import { ADD_ITEM, REMOVE_ITEM, REMOVE_ITEM_ALL, CLEAR_ALL_ITEM } from "../actions/types";

const initialState = {
  items: [],
  recent: 0
};

const itemReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM: {
      let items = cloneObject(state.items);
      let item = action.payload.value;
      let feeShip = action.payload.feeShip;
      var isAdded = false;
      var recentIdx;
      for (var i = 0; i < items.length; i++) {
        if (items[i].value.id == item.id) {
          items[i].quantity++;
          recentIdx = i;
          isAdded = true;
        }
      }
      if (!isAdded) {
        items.unshift({
          value: item,
          quantity: 1,
          feeShip: feeShip
        });
        recentIdx = 0;
      }
      state = Object.assign({}, state, { items: items, recent: recentIdx });
      return state;
    }
    case CLEAR_ALL_ITEM: {
      state = Object.assign({}, state, initialState);
      return state;
    }
    case REMOVE_ITEM: {
      let items = cloneObject(state.items);
      let id = action.payload;
      var index;
      var isRemoved = false;
      var recentIdx;
      for (var i = 0; i < items.length; i++) {
        if (items[i].value.id == id) {
          if (items[i].quantity > 1) {
            items[i].quantity--;
            recentIdx = i;
            isRemoved = true;
          } else {
            index = i;
          }
        }
      }
      if (!isRemoved) {
        recentIdx = 0;
        items.splice(index, 1);
      }
      state = Object.assign({}, state, { items: items, recent: recentIdx });
      return state;
    }

    case REMOVE_ITEM_ALL: {
      let items = cloneObject(state.items);
      let id = action.payload;
      var recentIdx;
      for (var i = 0; i < items.length; i++) {
        if (items[i].value.id == id) {
          recentIdx = 0;
          items.splice(i, 1);
        }
      }
      state = Object.assign({}, state, { items: items, recent: recentIdx });
      return state;
    }
    default:
      return state;
  }
};

function cloneObject(object) {
  return JSON.parse(JSON.stringify(object));
}

export default itemReducer;
