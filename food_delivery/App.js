import React, { Component } from "react";
import { Easing, Animated } from "react-native";
import { Router, Scene, Stack, ActionConst } from "react-native-router-flux";
import Login from "./src/components/Login/Login";
import Register from "./src/components/Register/Register";
import ForgotPassword from "./src/components/ForgotPassword/ForgotPassword";
import Home from "./src/components/Home/Home";
import Detail from "./src/components/Detail/Detail";
import Basket from "./src/components/Basket/Basket";
import UserInfo from "./src/components/UserInfo/UserInfo";
import ChangeUserInfo from "./src/components/UserInfo/ChangeUserInfo";
import ChangePassword from "./src/components/UserInfo/ChangePassword";
import Icon from "react-native-vector-icons/FontAwesome5";
import { connect } from "react-redux";
import Notification from "./src/components/Notification/Notification";
import Checkout from "./src/components/Checkout/Checkout";
import OrderHistory from "./src/components/Order/OrderHistory";
import OrderDetail from "./src/components/Order/OrderDetail";

const TabBarIcon = ({ title, focused }) => {
  let image;
  let retIcon;
  switch (title) {
    case "Home":
      image = focused
        ? (retIcon = <Icon name="home" size={24} color="#2196F3" solid />)
        : (retIcon = <Icon color="#9E9E9E" name="home" size={24} solid />);
      break;
    case "Basket":
      image = focused
        ? (retIcon = (
            <Icon name="shopping-cart" size={24} color="#2196F3" solid />
          ))
        : (retIcon = (
            <Icon color="#9E9E9E" name="shopping-cart" size={24} solid />
          ));
      break;
    case "User":
      image = focused
        ? (retIcon = <Icon name="user" size={24} color="#2196F3" solid />)
        : (retIcon = <Icon color="#9E9E9E" name="user" size={24} solid />);
      break;
    case "Notification":
      image = focused
        ? (retIcon = <Icon name="bell" size={24} color="#2196F3" solid />)
        : (retIcon = <Icon color="#9E9E9E" name="bell" size={24} solid />);
      break;
  }

  return retIcon;
};

class App extends Component {
  render() {
    return (
      <Router>
        <Stack key="root" hideNavBar>
          <Scene
            key="login"
            component={Login}
            initial={!this.props.isLoggedIn}
          />
          <Scene key="register" component={Register} />
          <Scene key="forgotPassword" component={ForgotPassword} />

          <Scene
            key="tabbar"
            tabs
            hideNavBar
            showLabel={false}
            initial={this.props.isLoggedIn}
          >
            <Stack key="home" title="Home" hideNavBar icon={TabBarIcon}>
              <Scene key="restaurants" component={Home} hideNavBar />
              <Scene key="detail" component={Detail} hideNavBar />
            </Stack>
            <Stack key="basket" title="Basket" hideNavBar icon={TabBarIcon}>
              <Scene
                key="tabOrder"
                tabs
                hideNavBar
                tabBarPosition="top"
                showLabel={true}
                swipeEnabled={false}
              >
                <Scene
                  key="cart"
                  component={Basket}
                  title="Basket"
                  hideNavBar
                />
                <Scene
                  key="orderHistory"
                  component={OrderHistory}
                  title="Order History"
                  hideNavBar
                />
              </Scene>
              <Scene key="orderDetail" component={OrderDetail} hideNavBar />
              <Scene key="checkout" component={Checkout} hideNavBar/>
            </Stack>
            <Scene
              key="noti"
              component={Notification}
              hideNavBar
              title="Notification"
              icon={TabBarIcon}
            />
            <Stack key="info" title="User" hideNavBar icon={TabBarIcon}>
              <Scene key="userInfo" component={UserInfo} />
              <Scene key="changeUserInfo" component={ChangeUserInfo} />
              <Scene key="changePassword" component={ChangePassword} />
            </Stack>
          </Scene>
        </Stack>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return {
    id: state.account.id,
    token: state.account.token,
    isLoggedIn: state.account.isLoggedIn
  };
};

export default connect(mapStateToProps)(App);
